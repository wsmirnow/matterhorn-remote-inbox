/*
 * Copyright 2013-2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.mhri.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * VersionTools
 *
 * @date 08.07.2013
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class VersionUtils {

    /**
     * MHRI version.
     */
    private static String version = null;

    /**
     * Set te MHRI version. Should be called on bundle initialization.
     * @param version MHRI version with 'v'-prefix
     */
    static void setVersion(String version) {
        VersionUtils.version = version;
    }

    /**
     * Returns the current version + build (formatted)
     *
     * @return the current version + build (formatted)
     */
    public static String getCurrentVersion() {
        return VersionUtils.version;
    }

    /**
     * Compares 2 versions (both formatted)
     *
     * @param v1 version 1
     * @param v2 version 2
     * @return 0 = equal, 1 = v1 > v2, -1 = v2 > v1, -10 = error
     */
    public int compareVersions(String v1, String v2) {
        if (v1.contains("alpha") || v2.contains("alpha") || v1.contains("beta") || v2.contains("beta") || v1.contains("release candidate") || v2.contains("release candidate")) {
            return 0;
        }

        String versionPatternStr = "\\s*v?(?<versionPart1>\\d+)\\.(?<versionPart2>\\d+)\\.(?<versionPart3>\\d+)\\s*.*";
        Pattern versionPattern = Pattern.compile(versionPatternStr);
        Matcher v1Matcher = versionPattern.matcher(v1);
        if (!v1Matcher.matches()) {
//            throw new IllegalArgumentException("Invalid format for version '" + v1 + "'.");
            return -10;
        }
        Matcher v2Matcher = versionPattern.matcher(v2);
        if (!v2Matcher.matches()) {
//            throw new IllegalArgumentException("Invalid format for version '" + v2 + "'.");
            return -10;
        }

        int v1Part = Integer.parseInt(v1Matcher.group("versionPart1"));
        int v2Part = Integer.parseInt(v2Matcher.group("versionPart1"));
        if (v1Part > v2Part)
            return 1;
        else if (v1Part < v2Part)
            return -1;

        v1Part = Integer.parseInt(v1Matcher.group("versionPart2"));
        v2Part = Integer.parseInt(v2Matcher.group("versionPart2"));
        if (v1Part > v2Part)
            return 1;
        else if (v1Part < v2Part)
            return -1;

        v1Part = Integer.parseInt(v1Matcher.group("versionPart3"));
        v2Part = Integer.parseInt(v2Matcher.group("versionPart3"));
        if (v1Part > v2Part)
            return 1;
        else if (v1Part < v2Part)
            return -1;

        return 0;
    }
}
