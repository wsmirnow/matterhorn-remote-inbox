/*
 * Copyright 2013-2015 Waldemar Smirnow
 * All rights reserved.
 */
package de.calltopower.mhri.ingestclient.connection;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Http classes wraper.
 */
public class OpencastHttpConnection {
    private static final Logger logger = Logger.getLogger(OpencastHttpConnection.class);

    private HttpClient httpClient = null;
    private HttpContext httpContext = null;
    private CookieStore cookieStore = null;

    public OpencastHttpConnection() {
        this(new DefaultHttpClient());
    }

    public OpencastHttpConnection(HttpClient httpClient) {
        this(httpClient, new BasicCookieStore());
    }

    public OpencastHttpConnection(HttpClient httpClient, CookieStore cookieStore) {
        this(httpClient, cookieStore, new BasicHttpContext());
    }

    public OpencastHttpConnection(HttpClient httpClient, CookieStore cookieStore, HttpContext httpContext) {
        this.httpClient = httpClient;
        this.cookieStore = cookieStore;
        this.httpContext = httpContext;

        this.httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);
        this.httpContext.setAttribute(ClientContext.COOKIE_STORE, this.cookieStore);
    }

    public void release() {
        if (getCookieStore() != null) {
            getCookieStore().clear();
            cookieStore = null;
        }
        if (getHttpClient() != null) {
            if (getHttpClient() instanceof CloseableHttpClient) {
                try {
                    ((CloseableHttpClient)getHttpClient()).close();
                } catch (IOException ex) {
                    logger.error("Closing HTTP connection failed: {}", ex);
                }
            }
            getHttpClient().getConnectionManager().shutdown();
            httpClient = null;
        }
        if (getHttpContext() != null) {
            httpContext = null;
        }
    }

    public HttpResponse execute(HttpUriRequest request) throws IOException {
        if (request != null) {
            logger.debug(String.format("Execute %s request to %s",
                  new String[] { request.getMethod(), request.getURI().toString() }));
        }

        HttpResponse response = getHttpClient().execute(request, getHttpContext());
        if (response.getStatusLine().getStatusCode() == 301) {
            String location = response.getHeaders("Location")[0].getValue();
            logger.debug(String.format("Redirect %s request from %s to %s",
                    request.getMethod(), request.getURI().toString(), location));

            if (request instanceof HttpRequestBase) {
                try {
                    ((HttpRequestBase) request).setURI(new URI(location));
                    response = getHttpClient().execute(request, getHttpContext());
                } catch (URISyntaxException ex) {
                    logger.debug(ex.getMessage());
                }
            }
        }

        logger.debug(String.format("Status of %s request to %s is %d",
                request.getMethod(),
                request.getURI().toString(),
                response.getStatusLine().getStatusCode()));
        return response;
    }

    /**
     * @return the httpClient
     */
    public HttpClient getHttpClient() {
        return httpClient;
    }

    /**
     * @return the httpContext
     */
    public HttpContext getHttpContext() {
        return httpContext;
    }

    /**
     * @return the cookieStore
     */
    public CookieStore getCookieStore() {
        return cookieStore;
    }
}
