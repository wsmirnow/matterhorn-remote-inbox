/*
 * Copyright 2013-2015 Waldemar Smirnow
 * All rights reserved.
 */
package de.calltopower.mhri.ingestclient.connection;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.simple.DeserializationException;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.calltopower.mhri.ingestclient.api.IngestClientException;

/**
 * OpencastHttpConnection factory/utility class.
 */
public class OpencastHttpConnectionFactory {
    private static final Logger logger = Logger.getLogger(OpencastHttpConnectionFactory.class);

    /**
     * Create an authorized opencast http connection.
     *
     * @param serverUrl opencast server URL
     * @param username opencast user name
     * @param password opencast puser password
     * @return OpencastHttpConnection, wrapper for HttpClient and HttpContext, where authorization was done
     * @throws IngestClientException
     * @throws URISyntaxException
     */
    public static OpencastHttpConnection createAuthenticatedOpencastConnection(URL serverUrl, String username, String password)
            throws IngestClientException, URISyntaxException {

        OpencastHttpConnection connection = new OpencastHttpConnection();
        try {
            authenticateOpencastConnection(connection, serverUrl, username, password);
            return connection;
        } catch (ConnectionClosedException ex) {
            logger.error("NETWORK_ERROR -- connection closed", ex);
            connection.release();
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } catch (NoHttpResponseException ex) {
            logger.error("SERVER_ERROR -- dropped connection without any response. Maybe the server is under heavy load.");
            connection.release();
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (SocketException ex) {
            logger.error("SOCKET_ERROR -- there is an error creating or accessing a socket", ex);
            connection.release();
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (URISyntaxException ex) {
            logger.error("CLIENT_ERROR -- the requesting uri is not valid", ex);
            connection.release();
            throw ex;
        } catch (UnsupportedEncodingException ex) {
            logger.error("CLIENT_ERROR -- the character encoding is not supported", ex);
            connection.release();
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.CLIENT_ERROR);
        } catch (IOException ex) {
            logger.error("NETWORK_ERROR -- network stream is interrupted", ex);
            connection.release();
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        }
    }

    /**
     * Sign in user in opencast.
     *
     * @param connection wrapper for HttpClient and HttpContext
     * @param serverUrl opencast server URL
     * @param username opencast user name
     * @param password opencast user password
     * @return reponse from opencast server
     * @throws URISyntaxException
     * @throws MalformedURLException
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws IngestClientException
     */
    public static HttpResponse authenticateOpencastConnection(OpencastHttpConnection connection, URL serverUrl, String username, String password)
            throws URISyntaxException, MalformedURLException, UnsupportedEncodingException, IOException, IngestClientException {

        // POST /j_spring_security_check
        // j_username: Username
        // j_password: Password
        // Return value description: Cookie
        URI uri = new URL(serverUrl, "/j_spring_security_check").toURI();

        if (logger.isInfoEnabled()) {
            logger.info("IngestClientImpl::initHttpClient - About to send POST request: " + uri.toURL().toString());
        }

        HttpPost httpPost = new HttpPost(uri.toURL().toString());

        List<NameValuePair> nameValuePairs = new ArrayList<>(3);
        nameValuePairs.add(new BasicNameValuePair("j_username", username));
        nameValuePairs.add(new BasicNameValuePair("j_password", password));
        nameValuePairs.add(new BasicNameValuePair("submit", "Login"));
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

        HttpResponse response = null;
        try {
            response = connection.execute(httpPost);
            String location = response.getHeaders("Location")[0].getValue();

            if (logger.isInfoEnabled()) {
                logger.info("Status code: " + response.getStatusLine().getStatusCode() + ", Location: " + location);
            }
            int statCode = response.getStatusLine().getStatusCode();

            if (((statCode != 200) && (statCode != 302)) || location.endsWith("error")) {
                logger.error("Could not authenticate: IngestClientException(CLIENT_ERROR), Status Code: "
                        + response.getStatusLine().getStatusCode());
                throw new IngestClientException("Could not authenticate", IngestClientException.Type.CLIENT_ERROR);
            }
            return response;
        } finally {
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
    }

    /**
     * Should return tru if the user is signed in.
     * @param connection wrapper for HttpClient and HttpContext
     * @param serverUrl opencast server URL
     * @return true if user has any roles except ROLE_ANONYMOUS, false otherwise
     * @throws URISyntaxException
     * @throws MalformedURLException
     * @throws IOException
     * @throws DeserializationException
     */
    public static boolean isAuthenticated(OpencastHttpConnection connection, URL serverUrl)
            throws URISyntaxException, MalformedURLException, IOException, DeserializationException {

        URL uri = new URL(serverUrl, "/info/me.json");
        HttpGet _httpget = new HttpGet(uri.toString());
        try {
            HttpResponse response = connection.execute(_httpget);
            int statCode = response.getStatusLine().getStatusCode();
            if (statCode == 200) {
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {
                    InputStream instream = null;
                    BufferedReader ir = null;
                    String resp = "";
                    try {
                        instream = resEntity.getContent();
                        ir = new BufferedReader(new InputStreamReader(instream, "UTF-8"));
                        String line = "";
                        while (line != null) {
                            resp += line;
                            line = ir.readLine();
                        }
                    } finally {
                        if (ir != null) {
                            ir.close();
                        }
                        if (instream != null) {
                            instream.close();
                        }
                    }

                    if (!resp.isEmpty()) {
                        JsonObject meJsonObj = (JsonObject) Jsoner.deserialize(resp);
                        if (meJsonObj.containsKey("roles")) {
                            JsonArray userRoles = meJsonObj.getCollection("roles");
                            for (Object role : userRoles) {
                                if (role instanceof String
                                        && ((String) role).startsWith("ROLE_")
                                        && !"ROLE_ANONYMOUS".equals(role)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        } finally {
            if (_httpget != null) {
                _httpget.releaseConnection();
            }
        }
        return false;
    }

    /**
     * Create an URL from base URL and custom path or query
     *
     * @param base base URL to start with
     * @param path optional URL path or null
     * @param query optional URL query or null
     * @return URL with given path and/or query
     * @throws URISyntaxException if URL build failed
     * @throws MalformedURLException if URL build failed
     */
    public static URL createURL(URL base, String path, NameValuePair... query)
            throws URISyntaxException, MalformedURLException {

        URIBuilder uriBuilder = new URIBuilder(base.toURI());
        String p = base.getPath();
        if (p == null || p.isEmpty())
            p = "/";
        if (path != null) {
            p = p.concat(path);
        }
        uriBuilder.setPath(p);
        if (query != null && query.length > 0) {
            uriBuilder.addParameters(Arrays.asList(query));
        }

        return uriBuilder.build().toURL();
    }
}
