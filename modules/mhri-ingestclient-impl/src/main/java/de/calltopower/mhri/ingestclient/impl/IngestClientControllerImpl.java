/*
 * Copyright 2013-2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.mhri.ingestclient.impl;

import de.calltopower.mhri.ingestclient.api.IngestClient;
import de.calltopower.mhri.ingestclient.api.IngestClientController;
import de.calltopower.mhri.ingestclient.api.IngestClientException;
import de.calltopower.mhri.ingestclient.connection.OpencastHttpConnection;
import de.calltopower.mhri.ingestclient.connection.OpencastHttpConnectionFactory;
import de.calltopower.mhri.util.Constants;
import de.calltopower.mhri.util.ParseUtils;
import de.calltopower.mhri.util.conf.Configuration;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.simple.DeserializationException;
import org.osgi.service.component.ComponentContext;

/**
 * IngestClientControllerImpl - Implements IngestClientController
 *
 * @date 19.03.2012
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
@Component(name = "de.calltopower.mhri.ingestclient.impl", immediate = true)
@Service
public class IngestClientControllerImpl implements IngestClientController {

    private static final Logger logger = Logger.getLogger(IngestClientControllerImpl.class);
    // authentication
    private String username = "";
    private String password = "";
    // REST endpoints URLs
    private static String series_list_URL = "/series/series";
    private static String series_URL = "/series";
    private static String series_create_URL = "/";
    private static String workflow_PATH = "/workflow";
    private static String workflow_definitions_PATH = "/definitions.json";
    // GET parameters for series
    private static String getparam_series_startpage = "startPage";
    private static String series_startpage = "0";
    private static String getparam_series_count = "count";
    private static String series_count = "100";
    // POST parameters for series
    private static String postparam_createSeries = "series";
    private static String postparam_createAcl = "acl";
    // misc
    @Reference
    private Configuration config;
    private URL serverURL = null;
    OpencastHttpConnection connection = null;
    private final ExecutorService threadPool = Executors.newCachedThreadPool(); // create new threads as needed, reuse previously constructed threads
    // Apache Commons
    private HttpPost httppost = null;
    private HttpGet httpget = null;
    // controller
    private List<IngestClient> list;

    /**
     * Set up
     *
     * @param cc ComponetnContext
     * @throws Exception
     */
    protected void activate(ComponentContext cc) throws Exception {
        if (logger.isInfoEnabled()) {
            logger.info("IngestClientControllerImpl::activate - Activating");
        }
        readProperties();

        String uName = config.get(Constants.PROPKEY_USERNAME);
        String pWord = config.get(Constants.PROPKEY_PASSWORD);
        setCredentials(uName, pWord);

        list = new LinkedList<>();
    }

    /**
     * Shut down http client and task executor when service is deactivated
     *
     * @param cc ComponentContext
     * @throws Exception
     */
    protected void deactivate(ComponentContext cc) throws Exception {
        releaseConnections();
        threadPool.shutdownNow();
    }

    private IngestClient getClient_internal(String pathToRecording) {
        for (IngestClient ic : list) {
            if (ic.getID().equals(pathToRecording)) {
                return ic;
            }
        }
        return null;
    }

    @Override
    public IngestClient getClient(String pathToRecording) {
        IngestClient ic = getClient_internal(pathToRecording);
        if (ic == null) {
            ic = new IngestClientImpl(config, pathToRecording);
            list.add(ic);
        }
        return ic;
    }

    @Override
    public boolean removeClient(String pathToRecording) {
        for (IngestClient ic : list) {
            if (ic.getID().equals(pathToRecording)) {
                ic.stopProcessing();
                list.remove(ic);
                return true;
            }
        }
        return false;
    }

    private void readProperties() {
        // read
        if (config.getProperties().containsKey(Constants.PROPKEY_SERVER_URL)) {
            try {
                String serverUrlStr = config.get(Constants.PROPKEY_SERVER_URL);
                if (!serverUrlStr.isEmpty() && !serverUrlStr.startsWith("http")) {
                    serverUrlStr = "http://".concat(serverUrlStr);
                }
                serverURL = new URL(serverUrlStr);
            } catch (IllegalArgumentException | MalformedURLException e) {
                logger.error("Can not parse server URL", e);
                try {
                    serverURL = new URL("http://localhost:8080");
                } catch (MalformedURLException ex) { }
            }
        } else if (config.getProperties().containsKey(Constants.PROPKEY_HOST)) {
            int port = -1;
            if (config.getProperties().containsKey(Constants.PROPKEY_PORT)) {
                try {
                    port = Integer.parseInt(config.get(Constants.PROPKEY_PORT));
                } catch (NumberFormatException e) {
                    logger.warn("Can not parse server port (value: " + config.get(Constants.PROPKEY_PORT) + ")");
                }
            }
            try {
                serverURL = new URI("http", null, config.get(Constants.PROPKEY_HOST), port, null, null, null).toURL();
            } catch (URISyntaxException | MalformedURLException ex) {
                logger.warn("Can not parse server hostname (value: " + config.get(Constants.PROPKEY_HOST) + ")");
            }
        }

        series_list_URL = config.get(Constants.PROPKEY_SERIES_LIST_URL);
        series_URL = config.get(Constants.PROPKEY_SERIES);
        series_create_URL = config.get(Constants.PROPKEY_SERIES_CREATE);

        workflow_PATH = config.get(Constants.PROPKEY_GETPARAM_WORKFLOW);
        workflow_definitions_PATH = config.get(Constants.PROPKEY_GETPARAM_WORKFLOW_DEFINITIONS);

        getparam_series_startpage = config.get(Constants.PROPKEY_GETPARAM_SERIES_STARTPAGE);
        series_startpage = config.get(Constants.PROPKEY_SERIES_STARTPAGE);
        getparam_series_count = config.get(Constants.PROPKEY_GETPARAM_SERIES_COUNT);
        series_count = config.get(Constants.PROPKEY_SERIES_COUNT);
        postparam_createSeries = config.get(Constants.PROPKEY_POSTPARAM_SERIES_CREATE_SERIES);
        postparam_createAcl = config.get(Constants.PROPKEY_POSTPARAM_SERIES_CREATE_ACL);
    }

    private void releaseConnections() {
        if (httpget != null) {
            httpget.releaseConnection();
        }
        if (httppost != null) {
            httppost.releaseConnection();
        }
        if (connection != null) {
            connection.release();
            connection = null;
        }
        list.clear();
    }

    private void setCredentials(String _username, String _password) {
        this.username = _username;
        this.password = _password;
    }

     public String getACL(String role) {
        String acl = "";
        acl += "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
        acl += "<acl xmlns=\"http://org.opencastproject.security\">";
        acl += "<ace>";
        acl += "<role>" + role + "</role>";
        acl += "<action>read</action>";
        acl += "<allow>true</allow>";
        acl += "</ace>";
        acl += "<ace>";
        acl += "<role>" + role + "</role>";
        acl += "<action>contribute</action>";
        acl += "<allow>true</allow>";
        acl += "</ace>";
        acl += "<ace>";
        acl += "<role>" + role + "</role>";
        acl += "<action>write</action>";
        acl += "<allow>true</allow>";
        acl += "</ace>";
        acl += "</acl>";
        return acl;
    }

    @Override
    public String createNewSeries(String document) throws IngestClientException, URISyntaxException {
        connection = OpencastHttpConnectionFactory.createAuthenticatedOpencastConnection(
                this.serverURL, this.username, this.password);
        try {
            // POST /series/
            // parameter:
            // series: The series document
            // acl: The access control list for the series

            URL url = OpencastHttpConnectionFactory.createURL(this.serverURL, series_URL + series_create_URL, null);
            httpget = new HttpGet(url.toString());
            if (logger.isInfoEnabled()) {
                logger.info("IngestClientControllerImpl::createNewSeries - About to send POST request: " + url.toString());
            }

            String acl_str = getACL("ROLE_ADMIN"); // TODO: Get role from info/me.json

            httppost = new HttpPost(url.toString());
            StringBody series = new StringBody(document, Charset.forName("UTF-8"));
            StringBody acl = new StringBody(acl_str, Charset.forName("UTF-8"));
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart(postparam_createSeries, series);
            reqEntity.addPart(postparam_createAcl, acl);
            httppost.setEntity(reqEntity);
            HttpResponse response = connection.execute(httppost);

            String ret = "";
            HttpEntity resEntity = response.getEntity();
            int statCode = response.getStatusLine().getStatusCode();
            if (logger.isInfoEnabled()) {
                logger.info("IngestClientControllerImpl::createNewSeries - Status code: " + statCode);
            }
            // created
            if ((resEntity != null) && (statCode == 201)) {
                InputStream instream = null;
                BufferedReader ir = null;
                try {
                    instream = resEntity.getContent();
                    ir = new BufferedReader(new InputStreamReader(instream, "UTF-8"));
                    String line = "";
                    String resp = "";
                    while (line != null) {
                        resp += line;
                        line = ir.readLine();
                    }
                    ret = resp;
                } catch (IOException ex) {
                    logger.error("IngestClientControllerImpl::createNewSeries - Caught IOException: " + ex.getMessage());
                    throw ex;
                } catch (RuntimeException ex) {
                    httppost.abort();
                    logger.error("IngestClientControllerImpl::createNewSeries - Caught RuntimeException: " + ex.getMessage());
                    throw ex;
                } finally {
                    EntityUtils.consume(resEntity);
                    EntityUtils.consume(reqEntity);
                    if (instream != null) {
                        instream.close();
                    }
                    if (ir != null) {
                        ir.close();
                    }
                }
            } // updated
            else if (statCode == 204) {
                return "";
            } else {
                logger.error("IngestClientControllerImpl::createNewSeries - IngestClientException(NETWORK_ERROR) 1");
                throw new IngestClientException(
                        "Got "
                        + response.getStatusLine().getStatusCode()
                        + " when attempting to start processing (GET).", IngestClientException.Type.NETWORK_ERROR);
            }

            return ret;
        } catch (URISyntaxException ex) {
            logger.error("IngestClientControllerImpl::createNewSeries - IngestClientException(CLIENT_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.CLIENT_ERROR);
        } catch (ConnectionClosedException ex) {
            logger.error("IngestClientControllerImpl::createNewSeries - IngestClientException(NETWORK_ERROR)"
                    + "-- connection closed");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } catch (NoHttpResponseException ex) {
            logger.error("IngestClientControllerImpl::createNewSeries - IngestClientException(SERVER_ERROR)"
                    + "-- dropped connection without any response. Maybe the server is under heavy load.");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (SocketException ex) {
            logger.error("IngestClientControllerImpl::createNewSeries - IngestClientException(SERVER_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (IOException ex) {
            logger.error("IngestClientControllerImpl::createNewSeries - IngestClientException(NETWORK_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } finally {
            releaseConnections();
            if (connection != null)
                connection.release();
        }
    }

    @Override
    public NetworkConnectionState getNetworkConnectionState() {
        OpencastHttpConnection connection = null;
        try {
            connection = OpencastHttpConnectionFactory.createAuthenticatedOpencastConnection(
                    this.serverURL, this.username, this.password);
            if (!OpencastHttpConnectionFactory.isAuthenticated(connection, this.serverURL)) {
                return NetworkConnectionState.WRONG_CREDENTIALS;
            }
            return NetworkConnectionState.ONLINE;
        } catch (URISyntaxException ex) {
            return NetworkConnectionState.ERROR;
        } catch (UnsupportedEncodingException ex) {
            return NetworkConnectionState.ERROR;
        } catch (IOException ex) {
            return NetworkConnectionState.CLIENT_OFFLINE;
        } catch (IngestClientException ex) {
            return NetworkConnectionState.SERVER_OFFLINE;
        } catch (DeserializationException ex) {
            return NetworkConnectionState.ERROR;
        } finally {
            if (connection != null)
                connection.release();
        }
    }

    // getter
    @Override
    public String getSeriesCatalog(String seriesID) throws IngestClientException, URISyntaxException {
        OpencastHttpConnection connection = null;
        try {
            connection = OpencastHttpConnectionFactory.createAuthenticatedOpencastConnection(
                    this.serverURL, this.username, this.password);

            // GET /seriesID.format, format = {xml, json}
            URL url = OpencastHttpConnectionFactory.createURL(this.serverURL, series_URL + "/" + seriesID + Constants.SERIES_CATALOG_FORMAT, null);
            httpget = new HttpGet(url.toString());
            if (logger.isInfoEnabled()) {
                logger.info("IngestClientControllerImpl::getSeriesCatalog - About to send GET request: " + url.toString());
            }
            HttpResponse response_get = connection.execute(httpget);

            if (logger.isInfoEnabled()) {
                logger.info("IngestClientControllerImpl::getSeriesCatalog - Status code: " + response_get.getStatusLine().getStatusCode());
            }
            String series = "";
            if (response_get.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity_get = response_get.getEntity();
                if (entity_get != null) {

                    BufferedReader reader = null;
                    InputStream instream = null;
                    try {
                        instream = entity_get.getContent();
                        reader = new BufferedReader(new InputStreamReader(instream, "UTF-8"));
                        String line = "";
                        while (line != null) {
                            series += line;
                            line = reader.readLine();
                        }
                    } catch (IOException ex) {
                        logger.error("IngestClientimpl::getSeriesCatalog - Caught IOException 1: " + ex.getMessage());
                    } catch (RuntimeException ex) {
                        httpget.abort();
                        logger.error("IngestClientimpl::getSeriesCatalog - Caught RuntimeException 1: " + ex.getMessage());
                    } finally {
                        EntityUtils.consume(entity_get);
                        if (instream != null) {
                            instream.close();
                        }
                        if (reader != null) {
                            reader.close();
                        }
                    }
                } else {
                    logger.error("IngestClientControllerImpl::getSeriesCatalog - IngestClientException(CLIENT_ERROR)");
                    throw new IngestClientException("Entity is null", IngestClientException.Type.CLIENT_ERROR);
                }
            } else {
                logger.error("IngestClientControllerImpl::getSeriesCatalog - IngestClientException(NETWORK_ERROR) 1, Status Code: " + response_get.getStatusLine().getStatusCode());
                throw new IngestClientException(
                        "Got "
                        + response_get.getStatusLine().getStatusCode()
                        + " when attempting to start processing (GET).", IngestClientException.Type.NETWORK_ERROR);
            }

            return series;
        } catch (URISyntaxException ex) {
            logger.error("IngestClientControllerImpl::getSeriesCatalog - IngestClientException(CLIENT_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.CLIENT_ERROR);
        } catch (ConnectionClosedException ex) {
            logger.error("IngestClientControllerImpl::getSeriesCatalog - IngestClientException(NETWORK_ERROR)"
                    + "-- connection closed");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } catch (NoHttpResponseException ex) {
            logger.error("IngestClientControllerImpl::getSeriesCatalog - IngestClientException(SERVER_ERROR)"
                    + "-- dropped connection without any response. Maybe the server is under heavy load.");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (SocketException ex) {
            logger.error("IngestClientControllerImpl::getSeriesCatalog - IngestClientException(SERVER_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (IOException ex) {
            logger.error("IngestClientControllerImpl::getSeriesCatalog - IngestClientException(NETWORK_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } finally {
            releaseConnections();
            if (connection != null)
                connection.release();
        }
    }

    @Override
    public HashMap<String, String> getSeriesList() throws IOException, IngestClientException, URISyntaxException {
        OpencastHttpConnection connection = null;
        try {
            connection = OpencastHttpConnectionFactory.createAuthenticatedOpencastConnection(
                    this.serverURL, this.username, this.password);

            // GET /series.format, format = {xml, json}
            URL url = OpencastHttpConnectionFactory.createURL(this.serverURL,
                    IngestClientControllerImpl.series_list_URL + Constants.SERIES_LIST_FORMAT,
                    new BasicNameValuePair(IngestClientControllerImpl.getparam_series_startpage, IngestClientControllerImpl.series_startpage),
                    new BasicNameValuePair(IngestClientControllerImpl.getparam_series_count, IngestClientControllerImpl.series_count));
            httpget = new HttpGet(url.toString());
            if (logger.isInfoEnabled()) {
                logger.info("IngestClientControllerImpl::getSeriesList - About to send GET request: " + url.toString());
            }
            HttpResponse response_get = connection.execute(httpget);

            if (logger.isInfoEnabled()) {
                logger.info("IngestClientControllerImpl::getSeriesList - Status code: " + response_get.getStatusLine().getStatusCode());
            }
            String seriesList = "";
            if (response_get.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity_get = response_get.getEntity();
                if (entity_get != null) {

                    InputStream instream = null;
                    BufferedReader reader = null;
                    try {
                        instream = entity_get.getContent();
                        reader = new BufferedReader(new InputStreamReader(instream, "UTF-8"));
                        String line = "";
                        while (line != null) {
                            seriesList += line;
                            line = reader.readLine();
                        }
                    } catch (IOException ex) {
                        logger.error("IngestClientimpl::getSeriesList - Caught IOException 1: " + ex.getMessage());
                    } catch (RuntimeException ex) {
                        httpget.abort();
                        logger.error("IngestClientimpl::getSeriesList - Caught RuntimeException 1: " + ex.getMessage());
                    } finally {
                        EntityUtils.consume(entity_get);
                        if (instream != null) {
                            instream.close();
                        }
                        if (reader != null) {
                            reader.close();
                        }
                    }
                } else {
                    logger.error("IngestClientControllerImpl::getSeriesList - IngestClientException(CLIENT_ERROR)");
                    throw new IngestClientException("Entity is null", IngestClientException.Type.CLIENT_ERROR);
                }
            } else {
                logger.error("IngestClientControllerImpl::getSeriesList - IngestClientException(NETWORK_ERROR) 1, Status Code: " + response_get.getStatusLine().getStatusCode());
                throw new IngestClientException(
                        "Got "
                        + response_get.getStatusLine().getStatusCode()
                        + " when attempting to start processing (GET).", IngestClientException.Type.NETWORK_ERROR);
            }
            if (seriesList.isEmpty()) {
                logger.error("IngestClientControllerImpl::getSeriesList - IngestClientException(NETWORK_ERROR) 1");
                throw new IngestClientException("No Series List could be retrieved.", IngestClientException.Type.NETWORK_ERROR);
            }

            return ParseUtils.getInstance().parseSeriesList(seriesList);
        } catch (URISyntaxException ex) {
            logger.error("IngestClientControllerImpl::getSeriesList - IngestClientException(CLIENT_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.CLIENT_ERROR);
        } catch (ConnectionClosedException ex) {
            logger.error("IngestClientControllerImpl::getSeriesList - IngestClientException(NETWORK_ERROR)"
                    + "-- connection closed");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } catch (NoHttpResponseException ex) {
            logger.error("IngestClientControllerImpl::getSeriesList - IngestClientException(SERVER_ERROR)"
                    + "-- dropped connection without any response. Maybe the server is under heavy load.");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (SocketException ex) {
            logger.error("IngestClientControllerImpl::getSeriesList - IngestClientException(SERVER_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (IOException ex) {
            logger.error("IngestClientControllerImpl::getSeriesList - IngestClientException(NETWORK_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } finally {
            releaseConnections();
            if (connection != null)
                connection.release();
        }
    }

    @Override
    public List<String> getWorkflowList() throws IOException, IngestClientException, URISyntaxException {
        OpencastHttpConnection connection = null;
        try {
            connection = OpencastHttpConnectionFactory.createAuthenticatedOpencastConnection(
                    this.serverURL, this.username, this.password);

            if (IngestClientControllerImpl.workflow_definitions_PATH .endsWith(".xml")) {
                // we can parse json only
                IngestClientControllerImpl.workflow_definitions_PATH = IngestClientControllerImpl.workflow_definitions_PATH.replace(".xml", ".json");
            }
            // GET /definitions.format, format = {xml, json}
            URL url = OpencastHttpConnectionFactory.createURL(this.serverURL,
                    IngestClientControllerImpl.workflow_PATH + IngestClientControllerImpl.workflow_definitions_PATH, null);
            httpget = new HttpGet(url.toString());
            if (logger.isInfoEnabled()) {
                logger.info("IngestClientControllerImpl::getWorkflowList - About to send GET request: " + url.toString());
            }
            HttpResponse response_get = connection.execute(httpget);

            if (logger.isInfoEnabled()) {
                logger.info("IngestClientControllerImpl::getWorkflowList - Status code: " + response_get.getStatusLine().getStatusCode());
            }
            String workflowList = "";
            if (response_get.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity_get = response_get.getEntity();
                if (entity_get != null) {

                    InputStream instream = null;
                    BufferedReader reader = null;
                    try {
                        instream = entity_get.getContent();
                        reader = new BufferedReader(new InputStreamReader(instream, "UTF-8"));
                        String line = "";
                        while (line != null) {
                            workflowList += line;
                            line = reader.readLine();
                        }
                    } catch (IOException ex) {
                        logger.error("IngestClientimpl::getWorkflowList - Caught IOException 1: " + ex.getMessage());
                    } catch (RuntimeException ex) {
                        httpget.abort();
                        logger.error("IngestClientimpl::getWorkflowList - Caught RuntimeException 1: " + ex.getMessage());
                    } finally {
                        EntityUtils.consume(entity_get);
                        if (instream != null) {
                            instream.close();
                        }
                        if (reader != null) {
                            reader.close();
                        }
                    }
                } else {
                    logger.error("IngestClientControllerImpl::getWorkflowList - IngestClientException(CLIENT_ERROR)");
                    throw new IngestClientException("Entity is null", IngestClientException.Type.CLIENT_ERROR);
                }
            } else {
                logger.error("IngestClientControllerImpl::getWorkflowList - IngestClientException(NETWORK_ERROR) 1, Status Code: " + response_get.getStatusLine().getStatusCode());
                throw new IngestClientException(
                        "Got "
                        + response_get.getStatusLine().getStatusCode()
                        + " when attempting to start processing (GET).", IngestClientException.Type.NETWORK_ERROR);
            }
            if (workflowList.isEmpty()) {
                logger.error("IngestClientControllerImpl::getWorkflowList - IngestClientException(NETWORK_ERROR) 1");
                throw new IngestClientException("No Series List could be retrieved.", IngestClientException.Type.NETWORK_ERROR);
            }

            return ParseUtils.getInstance().parseWorkflowList(workflowList);
        } catch (URISyntaxException ex) {
            logger.error("IngestClientControllerImpl::getWorkflowList - IngestClientException(CLIENT_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.CLIENT_ERROR);
        } catch (ConnectionClosedException ex) {
            logger.error("IngestClientControllerImpl::getWorkflowList - IngestClientException(NETWORK_ERROR)"
                    + "-- connection closed");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } catch (NoHttpResponseException ex) {
            logger.error("IngestClientControllerImpl::getWorkflowList - IngestClientException(SERVER_ERROR)"
                    + "-- dropped connection without any response. Maybe the server is under heavy load.");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (SocketException ex) {
            logger.error("IngestClientControllerImpl::getWorkflowList - IngestClientException(SERVER_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
        } catch (IOException ex) {
            logger.error("IngestClientControllerImpl::getWorkflowList - IngestClientException(NETWORK_ERROR)");
            throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
        } finally {
            releaseConnections();
            if (connection != null)
                connection.release();
        }
    }

    @Override
    public void reload() {
        releaseConnections();
        readProperties();

        String uName = config.get(Constants.PROPKEY_USERNAME);
        String pWord = config.get(Constants.PROPKEY_PASSWORD);
        setCredentials(uName, pWord);
    }
}
