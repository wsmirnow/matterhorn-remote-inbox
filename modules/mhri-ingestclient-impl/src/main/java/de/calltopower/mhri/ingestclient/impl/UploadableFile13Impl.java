/*
 * Copyright 2013-2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.mhri.ingestclient.impl;

import de.calltopower.mhri.application.api.RecordingFile;
import de.calltopower.mhri.ingestclient.api.IngestClientException;
import de.calltopower.mhri.ingestclient.api.UploadableFile13;
import de.calltopower.mhri.ingestclient.connection.OpencastHttpConnection;
import de.calltopower.mhri.ingestclient.connection.OpencastHttpConnectionFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

/**
 * UploadableFile13Impl - Implements UploadableFile for MH 1.3
 *
 * @date 24.09.2012
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public final class UploadableFile13Impl implements UploadableFile13 {

    private static final Logger logger = Logger.getLogger(UploadableFile13Impl.class);
    // URLs
    private String addTrackURL = "/ingest/addTrack";
    // POST parameters
    private String postparam_ingest_flavor = "flavor";
    private String postparam_ingest_mediapackage = "mediapackage";
    private String postparam_ingest_file = "file";
    // misc 'global'
    private URL serverURL = null;
    private String username = "";
    private String password = "";
    // misc 'local'
    private boolean isUploading = false;
    // misc
    private RecordingFile file = null;
    private File uploadFile = null;
    // Apache Commons
    private HttpPost httppost = null;

    public UploadableFile13Impl(
            RecordingFile file) throws IOException {
        this.file = file;
        this.uploadFile = new File(this.file.getPath());
    }

    public void setup(
            URL serverUrl,
            String password,
            String addTrackURL) throws IngestClientException {
        setup(serverUrl, username, password, addTrackURL);
    }

    public void setup(
            URL serverUrl,
            String username,
            String password,
            String addTrackURL) throws IngestClientException {
        this.serverURL = serverUrl;
        this.username = username;
        this.password = password;
        this.addTrackURL = addTrackURL;
    }

    public void setPostParameters(
            String postparam_ingest_flavor,
            String postparam_ingest_mediapackage,
            String postparam_ingest_file) {
        this.postparam_ingest_flavor = postparam_ingest_flavor;
        this.postparam_ingest_mediapackage = postparam_ingest_mediapackage;
        this.postparam_ingest_file = postparam_ingest_file;
    }

    private void releaseConnections() {
        if (httppost != null) {
            httppost.releaseConnection();
        }
    }

    // getter
    @Override
    public String getState() throws URISyntaxException, IngestClientException {
        String ret = "";

        return ret;
    }

    // misc
    @Override
    public boolean isUploading() throws IngestClientException {
        return isUploading;
    }

    @Override
    public void stopUpload() {
        if (logger.isInfoEnabled()) {
            logger.info("UploadableFile13Impl::stopUpload: Stopping upload");
        }
        releaseConnections();
        if (httppost != null) {
            httppost.abort();
            httppost = null;
        }
    }

    @Override
    public String upload() throws FileNotFoundException, IOException, URISyntaxException, IngestClientException {
        String mediaPackageXML_new = "";
        if ((this.file != null) && this.uploadFile.isFile()
                && !isUploading()
                && this.serverURL != null) {
            isUploading = true;
            mediaPackageXML_new = this.file.getRecording().getMediaPackage();

            this.file.getRecording().setUploadProgress(this.file.getRecording().getUploadProgress() + 10);

            // POST /addTrack
            // flavor: The kind of media track
            // mediaPackage: The media package as XML
            // file: The media track file
            OpencastHttpConnection connection = null;
            try {
                connection = OpencastHttpConnectionFactory.createAuthenticatedOpencastConnection(this.serverURL, this.username, this.password);
                URL url = OpencastHttpConnectionFactory.createURL(this.serverURL, this.addTrackURL, null);

                httppost = new HttpPost(url.toString());

                FileBody bin = new FileBody(this.uploadFile);
                StringBody flavor = new StringBody(this.file.getFlavor(), Charset.forName("UTF-8"));
                StringBody mediapackage = new StringBody(this.file.getRecording().getMediaPackage(), Charset.forName("UTF-8"));

                if (logger.isInfoEnabled()) {
                    logger.info("UploadableFile13Impl::upload - About to send POST request: " + url.toString());
                }

                MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                reqEntity.addPart(postparam_ingest_flavor, flavor);
                reqEntity.addPart(postparam_ingest_mediapackage, mediapackage);
                reqEntity.addPart(postparam_ingest_file, bin);

                httppost.setEntity(reqEntity);

                HttpResponse response = connection.execute(httppost);

                if (logger.isInfoEnabled()) {
                    logger.info("UploadableFile13Impl::upload - Status code: " + response.getStatusLine().getStatusCode());
                }
                if (response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity resEntity = response.getEntity();
                    int r = resEntity.getContent().read();
                    InputStream stream = null;
                    if (r != -1) {
                        try {
                            stream = resEntity.getContent();
                            mediaPackageXML_new = IOUtils.toString(stream);

                            // small hack for correct parsing
                            if (!mediaPackageXML_new.startsWith("<")) {
                                mediaPackageXML_new = "<" + mediaPackageXML_new;
                            }
                        } catch (IOException ex) {
                            logger.error("UploadableFile13Impl::upload - IOException: " + ex.getMessage());
                        } finally {
                            if (stream != null) {
                                IOUtils.closeQuietly(stream);
                            }
                        }
                    }
                    EntityUtils.consume(resEntity);
                } else {
                    logger.error("UploadableFile13Impl::upload - IngestClientException(SERVER_ERROR), Status Code: " + response.getStatusLine().getStatusCode());
                    throw new IngestClientException("500 Internal Server Error", IngestClientException.Type.SERVER_ERROR);
                }
                return mediaPackageXML_new;
            } catch (URISyntaxException ex) {
                logger.error("UploadableFile13Impl::upload - Caught URISyntaxException: " + ex.getMessage());
                throw ex;
            } catch (ConnectionClosedException ex) {
                logger.error("UploadableFile13Impl::upload - IngestClientException(NETWORK_ERROR)"
                        + "-- connection closed");
                throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
            } catch (NoHttpResponseException ex) {
                logger.error("UploadableFile13Impl::upload - IngestClientException(SERVER_ERROR)"
                        + "-- dropped connection without any response. Maybe the server is under heavy load.");
                throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
            } catch (SocketException ex) {
                logger.error("UploadableFile13Impl::upload - IngestClientException(SERVER_ERROR)");
                throw new IngestClientException(ex.getMessage(), IngestClientException.Type.SERVER_ERROR);
            } catch (IOException ex) {
                logger.error("UploadableFile13Impl::upload - IngestClientException(NETWORK_ERROR)");
                throw new IngestClientException(ex.getMessage(), IngestClientException.Type.NETWORK_ERROR);
            } finally {
                isUploading = false;
                releaseConnections();
            }
        }
        return mediaPackageXML_new;
    }
}
