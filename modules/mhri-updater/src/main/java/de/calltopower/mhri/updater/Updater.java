/*
 * Copyright 2013-2015 Denis Meyer
 * All rights reserved.
 */
package de.calltopower.mhri.updater;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.calltopower.mhri.util.Constants;
import de.calltopower.mhri.util.conf.Configuration;

/**
 * Updater - Checks for version updates
 *
 * @date 28.06.2013
 *
 * @author Denis Meyer (calltopower88@gmail.com)
 */
public class Updater {

    private static final Logger logger = Logger.getLogger(Updater.class);
    // REST endpoints URLs
    private String version_URL = "https://zentrum.virtuos.uos.de";
    private String version_PORT = "-1";
    private String version_PATH = "/mhri/latest.csv";
    // misc
    private final Configuration config;

    public Updater(Configuration config) {
        this.config = config;
        readProperties();
    }

    private void readProperties() {
        // read
        version_URL = config.get(Constants.PROPKEY_JVERSIONSERVER_URL);
        version_PORT = config.get(Constants.PROPKEY_JVERSIONSERVER_PORT);
        version_PATH = config.get(Constants.PROPKEY_JVERSIONSERVER_PATH);
    }

    public String getCurrentlyAvailableVersion() throws IOException {
        if (logger.isInfoEnabled()) {
            logger.info("Updater::getCurrentlyAvailableVersion - Checking for newer version");
        }

        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create().useSystemProperties();
        CloseableHttpClient httpClient = httpClientBuilder.build();
        HttpHost httpHost = new HttpHost(getBaseURL(), getPort(), getSheme());
        HttpGet httpRequest = new HttpGet(version_PATH);
        CloseableHttpResponse httpResponse = null;
        HttpEntity responseEntity = null;
        InputStream responseContent = null;
        try {
            httpResponse = httpClient.execute(httpHost, httpRequest);
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                responseEntity = httpResponse.getEntity();
                responseContent = responseEntity.getContent();
                String responseString = IOUtils.toString(responseContent);
                return parseLatestVersion(responseString);
            }
        } catch (ClientProtocolException ex) {
            logger.error("Updater::getCurrentlyAvailableVersion - ClientProtocolException: " + ex.getMessage());
        } catch (IllegalStateException ex) {
            logger.error("Updater::getCurrentlyAvailableVersion - IllegalStateException: " + ex.getMessage());
        } finally {
            IOUtils.closeQuietly(responseContent);
            EntityUtils.consume(responseEntity);
            IOUtils.closeQuietly(httpResponse);
            httpRequest.releaseConnection();
            IOUtils.closeQuietly(httpClient);
        }

        return null;
    }

    private String getSheme() {
        return this.version_URL != null && this.version_URL.startsWith("https://") ? "https" : "http";
    }

    private String getBaseURL() {
        String s = this.version_URL;
        if (s.startsWith("http://")) {
            s = this.version_URL.substring(7);
        }
        if (s.startsWith("https://")) {
            s = this.version_URL.substring(8);
        }
        return s;
    }

    private int getPort() {
        int p = 80;
        try {
            p = Integer.parseInt(version_PORT);
        } catch (NumberFormatException ex) {
            logger.error("Updater::getPort - NumberFormatException, returning default Port 8088");
        }
        if (80== p || 443 == p)
            return -1;

        return p;
    }

    public void reload() {
        readProperties();
    }

    /***
     * Parse latest version csv file and return first version found.
     * CSV file should be in parsable format with ';' as collumn separator and version in first collumn.
     * @param latestVersionCsvFileContent
     * @return first version found (latest version should be on top)
     */
    private String parseLatestVersion(String latestVersionCsvFileContent) {
        if (latestVersionCsvFileContent == null || "".equals(latestVersionCsvFileContent.trim()))
            return null;

        String[] lines = latestVersionCsvFileContent.split("(\r\n|\n|\r)");
        for (String line : lines) {

            if (line.length() > 0) {
                String versionDataLine = line;
                if (line.contains(";"))
                  versionDataLine = line.split(";", 2)[0];

                Pattern versionPattern = Pattern.compile("\\s*v?(?<version>\\d+\\.\\d+(\\.\\d+)*)");
                Matcher versionMatcher = versionPattern.matcher(versionDataLine);
                if (versionMatcher.matches()) {
                    String latestVersion = "v" + versionMatcher.group("version");
                    return latestVersion;
                }
            }
        }
        return null;
    }
}
